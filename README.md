# ArquillianPoc

Arquillian proof of concept

## Introduction 
Arquillian is a Container-agnostic integration testing framework for Java EE. It uses concept of container adapters to allow it to execute test code with an specific test environment They run as Junit tests. Micro-services focused.

Most of the Java EE implementations have an adapter than can be used to perform the deployment of the archive under test and to execute and report on the results of the unit tests. Arquillian distinguishes three different types of containers:
* Remote: using a remote protocol like JMX
* Managed: remote containers, but their lifecycle is managed by Arquillian itself
* Embedded: local containers where tests are performed using local protocols

* Deployment Archives:
  * ShrinkWrap provides an API to create deployable *.jar, *.war, and *.ear files
  * Arquillian allows us to configure the test deployment using the `@Deployment` annotation on a method that returns a ShrinkWrap object.

* Annotations
  * `@Inject`
  * `@Resource`
  * `@EJB`
  * `Deployment(name="myname" order = 1)`, where order is the execution order of the deployments
  * `@Test @OperateOnDeployment("myname")` , so we can run tests on multiples deployments at the same time
  
## Project structure

* src/
  * main/
    * java/ – application source files
    * resources/ – application configuration files
  * test/
    * java/ – test source files
    * resources/ – `persistence.xml`, `arquillian.xml` and other test configuration files
* `pom.xml` – Maven build file

## Basic Setup

Maven dependencies
```xml
<!-- Integration tests -->
<dependency>
	<groupId>org.jboss.arquillian.junit</groupId>
	<artifactId>arquillian-junit-container</artifactId>
	<version>${arquillian.version}</version>
	<scope>test</scope>
</dependency>
<dependency>
	<groupId>org.jboss.arquillian</groupId>
	<artifactId>arquillian-bom</artifactId>
	<version>${arquillian.version}</version>
	<scope>test</scope>
	<type>pom</type>
</dependency>

<!-- Arquillian Jboss container -->
<dependency>
	<groupId>org.jboss.arquillian.container</groupId>
	<artifactId>arquillian-weld-ee-embedded-1.1</artifactId>
	<version>${arquillian.container.version}</version>
	<scope>test</scope>
</dependency>
<dependency>
	<groupId>org.jboss.weld</groupId>
	<artifactId>weld-core</artifactId>
	<version>${jBoss.version}</version>
	<scope>test</scope>
</dependency>
```


## Examples

### Testing a simple component
This case won't show much difference from a unit test, but will allow us to get the Archillian machinery up and running.

Simple CDI bean:
```java
public class MessageRepeaterComponent {
	private static final String DEFAULT_TEXT = "You have a message: ";
	public String repeatMessage(String message) {
		return DEFAULT_TEXT + message;
	}
}
```

Test file
* `JavaArchive` creates a mockup web archive called `test.war`, this file is deployed into the container and then is used by Arquillian to perform tests
```java
// specify the test runner
@RunWith(Arquillian.class)
public class ArquillianTest {

// run inside a container
@Deployment
public static JavaArchive createDeployment() {
  // use the Arquillian Java API to create the container
  return ShrinkWrap.create(JavaArchive.class)
    // load the classes needed in isolation
    .addClass(MessageRecorder.class)
    // empty manifest resource
    .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
}

// inject our component to the test
@Inject
private MessageRepeaterComponent instance;

// perform our test
@Test
	public void givenMessage_WhenMessageRepeaterComponentRepeatsMessage_ThenMessageReceived() {
		Assert.assertEquals(getDefaultText() + DUMMY_MESSAGE, instance.repeatMessage(DUMMY_MESSAGE));
	}
```


### Testing EJB with several injections

Basic class
```java
public class LowerCaseConversor {
	public String convert(String text) {
		return text.toLowerCase();
	}
}
```

Wrap the previous class to increase difficulty
```java
public class StringUtils {
	public LowerCaseConversor getLowerCaseConversor() {
		return new LowerCaseConversor();
	}
}
```

Expose it as a service
```java
@EJB
public class StringUtilsService {
    @Inject
    private Utils utils;
    
    public String getConvertedCaps(final String word){
        return utils.getLowerCase().convert(word);
    }
}
```

Add the integration test file
```java
@RunWith(Arquillian.class)
public class StringUtilsServiceTest {

  private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String DUMMY_MESSAGE_LOWERCASE = "this is a message test: hi there!";

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class)
      //load several classes
			.addClasses(StringUtilsService.class, StringUtils.class, LowerCaseConversor.class)
			.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private StringUtilsService instance;

	@Test
	public void givenMessage_WhenStringUtilsServiceGetConvertedCaps_ThenMessageReceived() {
	  Assert.assertEquals(DUMMY_MESSAGE_LOWERCASE, instance.getConvertedCaps(DUMMY_MESSAGE));
	}
}
```