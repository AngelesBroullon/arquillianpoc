package org.thalion.lowerCase;

import javax.inject.Inject;

/**
 * Wrapper for String procedures
 * 
 * @author Angeles Broullon
 *
 */
public class StringUtils {

	/**
	 * The conversor
	 */
	@Inject
	private LowerCaseConversor conversor;

	/**
	 * Retrieves the conversor
	 * 
	 * @return
	 */
	public String getLowerCaseConversion(String text) {
		return conversor.convert(text);
	}

}
