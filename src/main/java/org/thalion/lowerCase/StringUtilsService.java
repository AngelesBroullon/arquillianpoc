package org.thalion.lowerCase;

import javax.ejb.EJB;
import javax.inject.Inject;

/**
 * A service which allows an String conversion
 * 
 * @author Angeles Broullon
 *
 */
@EJB
public class StringUtilsService {

	/**
	 * The utils bean
	 */
	@Inject
	private StringUtils utils;

	/**
	 * Changes the text format to lower case
	 * @param text the text to convert
	 * @return the text converted to lower case
	 */
	public String getConvertedCaps(final String text) {
		return utils.getLowerCaseConversion(text);
	}

}
