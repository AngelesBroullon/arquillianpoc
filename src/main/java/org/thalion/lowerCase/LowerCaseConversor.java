package org.thalion.lowerCase;

/**
 * A dummy example which allows to convert text to lower case
 * 
 * @author Angeles Broullon
 *
 */
public class LowerCaseConversor {

	/**
	 * Convert text to lower case
 * 
	 * @param text the text to convert
	 * @return the text changed to lower case
	 */
	public String convert(String text) {
		return text.toLowerCase();
	}

}
