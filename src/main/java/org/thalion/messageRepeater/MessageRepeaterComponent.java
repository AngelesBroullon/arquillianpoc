package org.thalion.messageRepeater;

/**
 * Dummy component class which repetas a mesage after a previous text
 * 
 * @author Angeles Broullon
 *
 */
public class MessageRepeaterComponent {

	/**
	 * The default text previous to the message repetition
	 */
	private static final String DEFAULT_TEXT = "You have a message: ";

	/**
	 * Repeats the message passed as a parameter
	 * 
	 * @param message the message to repeat, as a String
	 * @return a custom text, plus the message
	 */
	public String repeatMessage(String message) {
		return DEFAULT_TEXT + message;
	}

}
