package org.thalion.unit.lowerCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.lowerCase.LowerCaseConversor;
import org.thalion.lowerCase.StringUtils;

/**
 * Unit test for the StringUtils
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StringUtilsTest {
	
	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String DUMMY_MESSAGE_LOWERCASE = "this is a message test: hi there!";
	
	@InjectMocks
	private StringUtils instance;
	
	@Mock
	private LowerCaseConversor conversor;
	
	@Before
	public void setUp() {
		Mockito.when(conversor.convert(Matchers.eq(DUMMY_MESSAGE))).thenReturn(DUMMY_MESSAGE_LOWERCASE);
	}
	
	@Test
	public void testConstructor() {
		Assert.assertNotNull(new StringUtils());
	}
	
	@Test
	public void getLowerCaseConversion() {
		Assert.assertEquals(DUMMY_MESSAGE_LOWERCASE, instance.getLowerCaseConversion(DUMMY_MESSAGE));
	}

}
