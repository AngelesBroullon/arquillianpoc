package org.thalion.unit.lowerCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.lowerCase.StringUtils;
import org.thalion.lowerCase.StringUtilsService;

/**
 * Unit test for the StringUtilsService
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StringUtilsServiceTest {

	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String DUMMY_MESSAGE_LOWERCASE = "this is a message test: hi there!";

	@InjectMocks
	private StringUtilsService instance;

	@Mock
	private StringUtils utils;

	@Before
	public void setUp() {
		Mockito.when(utils.getLowerCaseConversion(Matchers.eq(DUMMY_MESSAGE))).thenReturn(DUMMY_MESSAGE_LOWERCASE);
	}
	
	@Test
	public void testConstructor() {
		Assert.assertNotNull(new StringUtilsService());
	}

	@Test
	public void getConvertedCaps() {
		Assert.assertEquals(DUMMY_MESSAGE_LOWERCASE, instance.getConvertedCaps(DUMMY_MESSAGE));
	}

}
