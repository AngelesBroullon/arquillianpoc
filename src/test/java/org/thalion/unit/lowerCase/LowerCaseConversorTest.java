package org.thalion.unit.lowerCase;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thalion.lowerCase.LowerCaseConversor;

/**
 * Unit test for the LowerCaseConversor
 * 
 * @author Angeles Broullon
 *
 */
public class LowerCaseConversorTest {
	
	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String DUMMY_MESSAGE_LOWERCASE = "this is a message test: hi there!";
	
	private LowerCaseConversor instance;
	
	@Before
	public void setUp() {
		instance = new LowerCaseConversor();
	}
	
	@Test
	public void testConstructor() {
		Assert.assertNotNull(new LowerCaseConversor());
	}

	@Test
	public void convert() {
		Assert.assertEquals(DUMMY_MESSAGE_LOWERCASE, instance.convert(DUMMY_MESSAGE));
	}

}
