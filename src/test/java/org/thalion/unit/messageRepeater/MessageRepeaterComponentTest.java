package org.thalion.unit.messageRepeater;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thalion.messageRepeater.MessageRepeaterComponent;
import org.thalion.utils.ReflectionUtils;

/**
 * Unit test for the MessageRepeaterComponent
 * 
 * @author Angeles Broullon
 *
 */
public class MessageRepeaterComponentTest {

	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String FIELD_PRE_MESSAGE_TEXT_NAME = "DEFAULT_TEXT";

	private MessageRepeaterComponent instance;

	@Before
	public void setUp() {
		instance = new MessageRepeaterComponent();
	}
	
	@Test
	public void testConstructor() {
		Assert.assertNotNull(new MessageRepeaterComponent());
	}

	@Test
	public void repeatMessage() {
		Assert.assertEquals(getDefaultText() + DUMMY_MESSAGE, instance.repeatMessage(DUMMY_MESSAGE));
	}

	/**
	 * Automatically gets the message text from the DEFAULT_TEXT private constant on
	 * MessageRepeaterComponent
	 * 
	 * @return the message text from the DEFAULT_TEXT private constant
	 */
	private String getDefaultText() {
		Object value = ReflectionUtils.getFieldValue(instance, FIELD_PRE_MESSAGE_TEXT_NAME);
		if (value instanceof String) {
			return (String) ReflectionUtils.getFieldValue(instance, FIELD_PRE_MESSAGE_TEXT_NAME);
		} else {
			return null;
		}
	}

}
