package org.thalion.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Utils based on the reflection package for testing purposes
 * 
 * @author Angeles Broullon
 *
 */
public class ReflectionUtils {

	/**
	 * Retrieves the value of a field, even if it is private one
	 * 
	 * @return the value of a field, even if it is private one
	 */
	public static Object getFieldValue(Object instance, String fieldName) {
		try {
			Field field = instance.getClass().getDeclaredField(fieldName);
			if (Modifier.isPrivate(field.getModifiers())) {
				field.setAccessible(true);
			}
			return field.get(instance);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			return null;
		}
	}

}
