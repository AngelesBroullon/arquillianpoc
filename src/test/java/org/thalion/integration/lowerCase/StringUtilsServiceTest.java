package org.thalion.integration.lowerCase;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.thalion.lowerCase.LowerCaseConversor;
import org.thalion.lowerCase.StringUtils;
import org.thalion.lowerCase.StringUtilsService;

/**
 * Arquillian integration test for the StringUtilsService
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(Arquillian.class)
public class StringUtilsServiceTest {

	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String DUMMY_MESSAGE_LOWERCASE = "this is a message test: hi there!";

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class)
				.addClasses(StringUtilsService.class, StringUtils.class, LowerCaseConversor.class)
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private StringUtilsService instance;

	@Test
	public void givenMessage_WhenStringUtilsServiceGetConvertedCaps_ThenMessageReceived() {
		Assert.assertEquals(DUMMY_MESSAGE_LOWERCASE, instance.getConvertedCaps(DUMMY_MESSAGE));
	}

}
