package org.thalion.integration.messageRepeater;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.thalion.messageRepeater.MessageRepeaterComponent;
import org.thalion.utils.ReflectionUtils;

/**
 * Arquillian integration test for the MessageRepeaterComponent
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(Arquillian.class)
public class MessageRepeaterComponentTest {

	private static final String DUMMY_MESSAGE = "This is a message test: hi there!";
	private static final String FIELD_PRE_MESSAGE_TEXT_NAME = "DEFAULT_TEXT";

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClass(MessageRepeaterComponent.class)
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Inject
	private MessageRepeaterComponent instance;

	@Test
	public void givenMessage_WhenMessageRepeaterComponentRepeatsMessage_ThenMessageReceived() {
		Assert.assertEquals(getDefaultText() + DUMMY_MESSAGE, instance.repeatMessage(DUMMY_MESSAGE));
	}

	/**
	 * Automatically gets the message text from the DEFAULT_TEXT private constant on
	 * MessageRepeaterComponent
	 * 
	 * @return the message text from the DEFAULT_TEXT private constant
	 */
	private String getDefaultText() {
		Object value = ReflectionUtils.getFieldValue(instance, FIELD_PRE_MESSAGE_TEXT_NAME);
		if (value instanceof String) {
			return (String) ReflectionUtils.getFieldValue(instance, FIELD_PRE_MESSAGE_TEXT_NAME);
		} else {
			return null;
		}
	}

}
