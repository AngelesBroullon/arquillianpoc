package org.thalion.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Suite for all tests related to the lowerCase package, both unit tests with
 * mocks and integration tests with Arquillian
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ org.thalion.unit.lowerCase.LowerCaseConversorTest.class,
		org.thalion.unit.lowerCase.StringUtilsTest.class, org.thalion.unit.lowerCase.StringUtilsServiceTest.class,
		org.thalion.integration.lowerCase.StringUtilsServiceTest.class })
public class LowerCaseSuite {

}
