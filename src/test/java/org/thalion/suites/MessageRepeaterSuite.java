package org.thalion.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Suite for all tests related to the mesageRepeater package, both unit tests
 * with mocks and integration tests with Arquillian
 * 
 * @author Angeles Broullon
 *
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ org.thalion.unit.messageRepeater.MessageRepeaterComponentTest.class,
		org.thalion.integration.messageRepeater.MessageRepeaterComponentTest.class })
public class MessageRepeaterSuite {

}
